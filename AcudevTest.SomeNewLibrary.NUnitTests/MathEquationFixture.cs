﻿using System;
using NUnit.Framework;

namespace AcudevTest.SomeNewLibrary.NUnitTests
{
    [TestFixture]
    public class MathEquationFixture
    {
        private IMathEquation _mathEquation;

        [SetUp]
        public void Setup()
        {
            _mathEquation = new MathEquation();
        }

        [TestCase(4, 2)]
        [TestCase(13, 4)]
        [TestCase(355, 19)]
        [TestCase(Int32.MaxValue, 46341)]
        public void GetRoundedSquareRootCanCalculate(int value, int expectedResult)
        {
            var actulaResult = _mathEquation.GetRoundedSquareRoot(value);
            Assert.AreEqual(expectedResult, actulaResult);
        }
    }
}
