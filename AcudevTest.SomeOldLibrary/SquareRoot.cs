﻿using System;

namespace AcudevTest.SomeOldLibrary
{
    public class SquareRoot
    {
        public double Calculate(int value, bool round)
        {
            var result = Math.Sqrt(value);
            return round 
                ? Math.Round(result) 
                : result;
        }
    }
}
