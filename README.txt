Task 1:
no comments

Task 2:
The diagram is incorrect. 
Build method in diagram is indicate as a protected method, that means it can be called only from inheritance structure of Car classes implementation.
Logically this method should be virtual/abstract in base class, and in concrete class it should be override. 
Next step 
-> To create an instance of Car, logically we need to call method CarFactory.BuildCar(carType) 
-> which by some logic should call some concrete Car class (SmallCar, SedanCar, LuxuryCar) constructor.
-> concrete Car class constructor call the base class constructor where is incorrect to call the method Build 
   because the concrete class constructor is not executed yet.
   
So, how to fix this situation:
1. Car class shouldn't know anything related to CarFactory but inversely CarFactory.CarFactory method should know how to create an instance of Car.
2.a. Method Build should be public and CarFactory.BuildCar method after creation of Car instance should call the Build method.
2.b. Best solution is to have:
     - Car classes and CarFactory class in same assembly. 
	 - Car classes should have internal constructors and internal Build method.
	 - in result CarFactory.CarFactory  can create an instance of Car, also can call method Build. 
	 
Task 3:
open in browser site.html

Task 4:
I have implemented repository pattern BUT
I think repository patter is not correct to use for entity framework,  
because DbContext already implements repository pattern and unit of work as well.

Better solution is to create some interface  
e.g. 
1. IUnitOfWork{
		IDbSet<T> Set();
		SaveChanges();
   }
    
2. OurDbContext should implement this interface. and use it in service layer through interface

3. in service method (where we probably want to use IUnitOfWork), need to create an instance of unitofwork through  some factory method 
	using(IUnitOfWork uow = Factory.Method())
	{
		//logic related to data store
	}

4.  if is necessary to have transaction
	using (TransactionScope scope = new TransactionScope())
	{
		using(IUnitOfWork uow = Factory.Method())
		{		
			//logic related to data store
		}	
	}

I think the developer is responsible to create an instance of unitofwork and transaction,  
and not creating DbContext singleton or per web request.

      