(function($){
    var generateBtnSelector = '#btn'; 
    var gridSelector = '#grid';
    
    //document ready    
    $(function(){
        $(generateBtnSelector).click(generateBtnClick);
    });
    
    //
    function generateBtnClick(){
        var number = +$('#number').val();
		if(number < 1 || number > 100){
			alert('Number should be between 1 and 100.');
			return;
		}
		
        $(gridSelector).empty();        
        
        generateCells(number);
        foundCellPositions(number);        
    }
    
    function generateCells(number){
        var elements = $();
         for(var i = 1; i <= number; i++){
             var el = '<div class="cell cell'+i%2+'">' + i + '</div>';
             $(gridSelector).append(el);   
         }
    }
  
    function resetLineLength(lineLength, childrenLength, foundPosition){
        var remainsCellsCount = childrenLength - foundPosition; 
        if(lineLength > remainsCellsCount){
            lineLength = remainsCellsCount;
        }
        return lineLength;
    }
    
    var exp = 50;
    function animateCell(child, row, col, count, number){
		
		var coef = (130 - number) / (count * 1.3); 
		
        $(child).animate({top: row * 50 + 50, left : col * 50 + 50, opacity:1}, coef*count*count - count);
    }
    
    function foundCellPositions(number){
        var columns = Math.ceil(Math.sqrt(number));
        
        var row = Math.floor(columns / 2);
        var col = row - (columns + 1) % 2;
        
        var $children = $(gridSelector).children();
        animateCell($children[0], row, col, 0, number);
      
        var directionX = 1;  
        var directionY = -1;
        
        var childrenLength = $children.length;
        var foundPosition = 1;
        var lineLength = 1;
        while(childrenLength > foundPosition){

            lineLength = resetLineLength(lineLength, childrenLength, foundPosition);
                       
            //orizontal
            for(var i = 0; i < lineLength; i++){
                col += 1 * directionX;
                animateCell($children[foundPosition], row, col, foundPosition, number);
                foundPosition++;
            }
            directionX *= -1;
            
            lineLength = resetLineLength(lineLength, childrenLength, foundPosition);
            
            //vertical
            for(var i = 0; i < lineLength; i++){
                row += 1 * directionY;
                animateCell($children[foundPosition], row, col, foundPosition, number);
                foundPosition++;
            }
            directionY *= -1;
            
            lineLength++;
        }
    }
        
}(jQuery));

