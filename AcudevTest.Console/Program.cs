﻿using AcudevTest.Console.Enums;

namespace AcudevTest.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            CarFactory.BuildCar(CarType.Luxury);

            System.Console.ReadKey();
        }
    }
}
