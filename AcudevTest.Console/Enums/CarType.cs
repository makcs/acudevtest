﻿namespace AcudevTest.Console.Enums
{
    public enum CarType
    {
        Small,
        Sedan,
        Luxury
    }
}