﻿using System.ComponentModel;
using AcudevTest.Console.Enums;
using AcudevTest.Console.Models;

namespace AcudevTest.Console
{
    public class CarFactory
    {
        public static Car BuildCar(CarType carType)
        {
            Car car;
            switch (carType)
            {
                case CarType.Small:
                    car = new SmallCar();
                    break;
                case CarType.Sedan:
                    car = new SedanCar();
                    break;
                case CarType.Luxury:
                    car = new LuxuryCar();
                    break;
                default:
                    throw new InvalidEnumArgumentException();
            }
            car.Build();
            return car;
        }
    }
}