using AcudevTest.Console.Enums;

namespace AcudevTest.Console.Models
{
    public class SmallCar : Car
    {
        public SmallCar() : base(CarType.Small)
        {
        }

        internal override void Build()
        {
            System.Console.WriteLine("Building a Small car");
        }
    }
}