﻿using AcudevTest.Console.Enums;

namespace AcudevTest.Console.Models
{
    public class LuxuryCar : Car
    {
        public LuxuryCar() : base(CarType.Luxury)
        {
        }

        internal override void Build()
        {
            System.Console.WriteLine("Building a Luxury car");
        }
    }
}