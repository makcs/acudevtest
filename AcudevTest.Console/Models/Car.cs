﻿using AcudevTest.Console.Enums;

namespace AcudevTest.Console.Models
{
    public abstract class Car
    {
        internal abstract void Build();

        protected Car(CarType carType)
        {
        }
    }
}