using AcudevTest.Console.Enums;

namespace AcudevTest.Console.Models
{
    public class SedanCar : Car
    {
        public SedanCar() : base(CarType.Sedan)
        {
        }

        internal override void Build()
        {
            System.Console.WriteLine("Building a Sedan car");
        }
    }
}