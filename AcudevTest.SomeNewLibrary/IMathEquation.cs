﻿namespace AcudevTest.SomeNewLibrary
{
    public interface IMathEquation
    {
        int GetRoundedSquareRoot(int x);
    }
}