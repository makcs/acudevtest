﻿using AcudevTest.SomeOldLibrary;

namespace AcudevTest.SomeNewLibrary
{
    public class MathEquation : SquareRoot, IMathEquation
    {
        public int GetRoundedSquareRoot(int x)
        {
            var result = Calculate(x, true);
            return (int)result;
        }
    }
}