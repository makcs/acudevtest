﻿using System.Data.Entity;
using AcudevTest.Web.DomainModels;

namespace AcudevTest.Web.DAL
{
    public class MyContext : DbContext
    {
        public DbSet<User> Users { get; set; }
    }
}