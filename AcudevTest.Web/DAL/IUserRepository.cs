﻿using AcudevTest.Web.DomainModels;

namespace AcudevTest.Web.DAL
{
    public interface IUserRepository : IRepository<User>
    {
    }
}