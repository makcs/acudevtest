﻿using System;
using System.Linq;
using System.Linq.Expressions;
using AcudevTest.Web.DomainModels;

namespace AcudevTest.Web.DAL
{
    public interface IRepository<TEntity> where TEntity : Entity
    {
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate);
        void Add(TEntity entity);
        void Delete(TEntity entity);
        void Edit(TEntity entity);
        void SaveChanges();
    }
}