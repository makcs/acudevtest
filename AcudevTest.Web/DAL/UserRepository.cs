﻿using AcudevTest.Web.DomainModels;

namespace AcudevTest.Web.DAL
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(MyContext context) : base(context)
        {
        }
    }
}