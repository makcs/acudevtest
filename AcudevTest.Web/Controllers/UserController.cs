﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AcudevTest.Web.DAL;
using AcudevTest.Web.DomainModels;

namespace AcudevTest.Web.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserRepository _userRepository;

        public UserController(IUserRepository userRepository)
        {
            if (userRepository == null)
                throw new ArgumentNullException(nameof(userRepository));
            _userRepository = userRepository;
        }


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            var data = _userRepository.GetAll().ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Show(int id)
        {
            var data = _userRepository.GetAll().First(x => x.Id == id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Add(User user)
        {
            _userRepository.Add(user);
            _userRepository.SaveChanges();
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult Edit(User user, int id)
        {
            var userToUpdate = _userRepository.GetAll().First(x => x.Id == id);
            userToUpdate.FirstName = user.FirstName;
            userToUpdate.LastName = user.LastName;
            _userRepository.Edit(userToUpdate);
            _userRepository.SaveChanges();
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var userToDelete = _userRepository.GetAll().First(x => x.Id == id);
            _userRepository.Delete(userToDelete);
            _userRepository.SaveChanges();
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}