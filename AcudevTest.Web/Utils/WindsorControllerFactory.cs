﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace AcudevTest.Web.Utils
{
    public class WindsorControllerFactory : DefaultControllerFactory
    {
        public override void ReleaseController(IController controller)
        {
            IoC.Release(controller);
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null)
            {
                throw new HttpException(404,
                    $"The controller for path '{requestContext.HttpContext.Request.Path}' could not be found.");
            }
            return (IController)IoC.Resolve(controllerType);
        }
    }
}