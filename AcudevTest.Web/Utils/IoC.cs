﻿using System;
using AcudevTest.Web.DAL;
using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace AcudevTest.Web.Utils
{
    public static class IoC
    {
        private static readonly IWindsorContainer Container = new WindsorContainer();

        public static void RegisterTransient(Type typeBase)
        {
            Container.Register(Classes.FromThisAssembly().BasedOn(typeBase)
                .Configure(cfg => cfg.LifestyleTransient()));
        }

        public static void RegisterTransientWithServiceDefaultInterfaces(Type typeBase)
        {
            Container.Register(Classes.FromThisAssembly().BasedOn(typeBase).WithServiceDefaultInterfaces()
                .Configure(cfg => cfg.LifestyleTransient()));
        }

        public static void RegisterDbContext()
        {
            Container.Register(Component.For<MyContext>().LifeStyle.PerWebRequest);
        }

        public static void Release(object instance)
        {
            Container.Release(instance);
        }

        public static object Resolve(Type service)
        {
            return Container.Resolve(service);
        }
    }
}