﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AcudevTest.Web.DAL;
using AcudevTest.Web.Utils;

namespace AcudevTest.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            InitializeServiceLocator();
        }

        private void InitializeServiceLocator()
        {
            IoC.RegisterTransient(typeof(IController));
            IoC.RegisterTransientWithServiceDefaultInterfaces(typeof(IRepository<>));
            IoC.RegisterDbContext();

            ControllerBuilder.Current.SetControllerFactory(typeof(WindsorControllerFactory));
        }
    }
}
