﻿'use strict';
var services = angular.module('demo.services', ['ngResource']);

services.factory('usersFactory', function($resource) {
    return $resource('/User/List', {}, {
        query: {method: 'GET', isArray: true}
    });
});

services.factory('userFactory', function ($resource) {
    return $resource('/User/:action/:id', {}, {
        show: { method: 'GET', params: { action: 'Show' } },
        add: { method: 'POST', params: { action: 'Add' } },
        edit: { method: 'POST', params: { action: 'Edit', id: '@id' } },
        delete: { method: 'POST', params: { action: 'Delete', id: '@id' } }
    });
});