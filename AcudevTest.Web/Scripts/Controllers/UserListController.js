﻿'use strict';
var app = angular.module('demo');

app.controller('UserListController', ['$scope', 'usersFactory', 'userFactory', '$location', function($scope, usersFactory, userFactory, $location) {
    $scope.users = usersFactory.query();

    $scope.addUser = function() {
        $location.path('/AddUser');
    };

    $scope.editUser = function(userId) {
        $location.path('/EditUser/' + userId);
    };

    $scope.deleteUser = function(userId) {
        userFactory.delete({ id: userId }).$promise.then(function() {
            $scope.users = usersFactory.query();    
        });
    };

}]);