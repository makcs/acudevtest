﻿'use strict';
var app = angular.module('demo');

app.controller('AddUserController', ['$scope', 'userFactory', '$location', function($scope, userFactory, $location) {
    $scope.addUser = function () {
        userFactory.add($scope.user).$promise.then(function() {
            $location.path('/UserList');    
        });
    }
}]);