﻿'use strict';
var app = angular.module('demo');

app.controller('EditUserController', ['$scope', '$routeParams', 'userFactory', '$location', 
    function($scope, $routeParams, userFactory, $location) {
        $scope.editUser = function() {
            userFactory.edit($scope.user).$promise.then(function() {
                $location.path('/UserList');    
            });
        };

        $scope.cancel = function () {
            $location.path('/UserList');
        };

        $scope.user = userFactory.show({id: $routeParams.id});
    }]);