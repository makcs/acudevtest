﻿'use strict';
var app = angular.module('demo', ['demo.services', 'ngRoute']);

app.config(['$routeProvider',function($routeProvider) {
    $routeProvider
        .when('/UserList', { templateUrl: 'Static/UserList.html', controller: 'UserListController' })
        .when('/AddUser', { templateUrl: 'Static/AddUser.html', controller: 'AddUserController' })
        .when('/EditUser/:id', { templateUrl: 'Static/EditUser.html', controller: 'EditUserController' })
        .otherwise({ redirectTo: '/UserList' });
}]);