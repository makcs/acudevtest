﻿namespace AcudevTest.Web.DomainModels
{
    public class User : Entity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}