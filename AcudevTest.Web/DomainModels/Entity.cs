﻿namespace AcudevTest.Web.DomainModels
{
    public abstract class Entity
    {
        public int Id { get; private set; }
    }
}