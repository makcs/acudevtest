﻿using System;
using AcudevTest.Console.Enums;
using AcudevTest.Console.Models;
using NUnit.Framework;

namespace AcudevTest.Console.NUnitTests
{
    [TestFixture]
    public class CarFactoryFixture
    {
        [TestCase(CarType.Small, typeof(SmallCar))]
        [TestCase(CarType.Sedan, typeof(SedanCar))]
        [TestCase(CarType.Luxury, typeof(LuxuryCar))]
        public void CanBuildCar(CarType carType, Type expectedCarInstanceType)
        {
            var car = CarFactory.BuildCar(carType);
            Assert.IsInstanceOf(expectedCarInstanceType, car);
        }
    }
}
